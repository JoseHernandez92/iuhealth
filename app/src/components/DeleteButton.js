import Component from '../infrastructure/Component'
import { myBus } from '../infrastructure/myBus'

let template = `
  <style>
    div { 
      display: inline;
    }
    button {
      background-color: crimson;
      font-size: 12pt;
      border: none;
      display: inline;
      padding: 5px;
    }
  </style>
    
  <button id='delete'>Borrar todos</button>
`

export default class DeleteButton extends Component {
  constructor() {
    super(template, myBus)
    
    let button_element = this.shadowroot.querySelector('button')

    button_element.addEventListener('click', () => {
      this.bus.publish('delete.button.clicked')
    })

  }

}