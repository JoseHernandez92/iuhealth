import Component from '../infrastructure/Component'
import Item from './Item'
import { myBus } from '../infrastructure/myBus'

let template = `
<style>
  div { margin-top: 15px}
  .hidden {
    display: none;
  }
  table {
    border-spacing: 0px;
    border-collapse: collapse;
  }
  th { 
    border-bottom: 3px solid cyan;
    border-rigth: 0px;
    padding: 6px;
  }
  td {
    padding: 6px;
    border-bottom: 1px solid Fuchsia;
  }
</style>

<div>
  <table>
    <thead class="hidden">
      <th>Nombre</th>
      <th>Tipo</th>
    </thead>
    <tbody></tbody>
  </table>
</div>
`

export default class ItemsList extends Component {
  constructor() {
    super(template, myBus)
    this.pickElements(['thead', 'tbody'])
    Item.mount('item-component')
  }
  
  static get observedAttributes() {
    return ['items']
  }
  
  attributeChangedCallback() {
    this.render()
  }
  
  render() {
    let items = JSON.parse(this.getAttribute('items'))
    let tbody = this.elements.tbody

    while (tbody.firstChild) {
      tbody.removeChild(tbody.firstChild)
    }

    (items.length) ? 
      this.elements.thead.classList.remove('hidden')
      : this.elements.thead.classList.add('hidden')

      items.forEach(item => {
        let item_row = document.createElement('tr')
        let item_node = document.createElement('item-component')
        item_node.setAttribute("item", JSON.stringify(item))
        item_row.appendChild(item_node)
        this.elements.tbody.appendChild(item_row)
    })

  }
  
}