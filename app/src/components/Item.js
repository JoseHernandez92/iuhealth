import Component from "../infrastructure/Component"
import { myBus } from "../infrastructure/myBus"

let template = `
  <td id="name"></td>
  <td id="type"></td>
`

export default class Item extends Component {
  constructor(){
    super(template, myBus)
    this.pickElements(['#name', '#type'])
  }

  static get observedAttributes() {
    return ['item']
  }

  attributeChangedCallback(){
    this.render()
  }

  render() {
    let item = JSON.parse(this.getAttribute('item'))
    this.elements.name.innerHTML = item.name 
    this.elements.type.innerHTML = item.type
  }
}