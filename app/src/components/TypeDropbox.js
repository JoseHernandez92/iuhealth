import Component from '../infrastructure/Component'
import { myBus } from '../infrastructure/myBus'
import { service } from '../services/itemTypes'

let template = `
  <style>
    .field { margin-top: 10px;}
  </style>

  <div class="field">
    <label>Tipo:<l/abel>
    <select>
      <option value="">selecciona uno...</option>
    </select>
  </div>
`
export default class TypeDropbox extends Component {
  constructor() {
    super(template, myBus)
    this.pickElements(['select'])
    this.bus.subscribe('types.loaded', types => this.renderTypes(types))
    
    service(myBus).start()
    
    this.elements.select.addEventListener('change', 
      event => this.bus.publish('type.changed', event.target.value)
    )
  }

  renderTypes(types) {
    types.forEach(item => {
      let option = document.createElement("option")
      option.setAttribute('value', item.id)
      option.innerHTML = item.value
      this.elements.select.appendChild(option)
    })
  }
}