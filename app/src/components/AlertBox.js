import Component from '../infrastructure/Component'
import { myBus } from '../infrastructure/myBus'

let template = `
  <style>
    div { 
      background-color: tomato;
      color: white;
      margin-top: 10px;
      width: 300px;
    }
  </style>
  
  <div></div>
`

export default class AlertBox extends Component {
  constructor() {
    super(template, myBus)
    
    this.bus.subscribe('validation.error', error_message => 
      this.shadowroot.querySelector('div').innerHTML = error_message
    )
  }

}