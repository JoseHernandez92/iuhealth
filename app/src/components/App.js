import Component from '../infrastructure/Component'
import { myBus } from '../infrastructure/myBus'

import NameInput from './NameInput'
import AlertBox from './AlertBox'
import TypeDropbox from './TypeDropbox'
import ItemsList from './ItemsList'
import SaveButton from './SaveButton'
import DeleteButton from './DeleteButton'

NameInput.mount('name-input')
AlertBox.mount('alert-box')
TypeDropbox.mount('type-dropbox')
ItemsList.mount('items-list')
SaveButton.mount('save-button')
DeleteButton.mount('delete-button')

let template = `
  <style>
    .container {
      width: 50%;
      border: 1px solid white;
      align: auto;
      padding: 20px;
      float: left;
    }

    .button-container {
      margin-top: 25px;
    }

    img {
      margin-left: 20px;
    }

    ::slotted(div) {
      font-size: 22pt;
      font-weight: bold;
    }
  </style>

  <slot name='title'>Application title</slot>
  
  <div class="container">
    <name-input></name-input>
    <type-dropbox></type-dropbox>
    <alert-box></alert-box>
    <div class="button-container">
      <save-button></save-button>
      <delete-button></delete-button>
    </div>
    <items-list></items-list>
  </div>
    <img src="https://ilovepinto.es/wp-content/uploads/2019/04/semana-de-la-salud.gif" width="500"/>
`

export default class App extends Component {
  constructor() {
    super(template, myBus)
    this.item = {
      name: '',
      type: ''
    }
    this.items = []

    this.elements.itemList = this.shadowroot.querySelector('items-list')
    
    this.bus.subscribe('name.changed', name => this.item.name = name)
    this.bus.subscribe('type.changed', type => this.item.type = type)
    this.bus.subscribe('save.button.clicked', () => this.trySave())
    this.bus.subscribe('delete.button.clicked', () => this.delete())
  }

  showAlert() {
    let alert_message = 'No puedes salvar con un nombre vacío'
    this.bus.publish('validation.error', alert_message)
  }

  trySave(){
    if (!this.item.name) {
      this.showAlert()
      return
    }
    this.save()
  }
  
  save() {
    this.items.push(this.item)
    this.item = { name: '', type: '' }
    let items = JSON.stringify(this.items)
    this.elements.itemList.setAttribute('items', items)
    this.bus.publish('item.saved')
  }
  
  delete() {
    this.items = []
    let items = JSON.stringify(this.items)
    this.elements.itemList.setAttribute('items', items)
  }
  
}