import Component from '../infrastructure/Component'
import { myBus } from '../infrastructure/myBus'

let template = `
  <style>
    label { margin-right: 5px;}
  </style>

  <div class="container">
    <label class="field-label" for='name'>Nombre:</label>
    <input id='name' name='name_input'></input>
  </div>
`
export default class NameInput extends Component {
  constructor() {
    super(template, myBus)
    this.pickElements(['input'])
    this.elements.input.addEventListener('keyup', 
      event => this.bus.publish('name.changed', event.target.value)
    )

    this.bus.subscribe("item.saved", () => this.elements.input.value = '')
  }
}