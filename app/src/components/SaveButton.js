import Component from '../infrastructure/Component'
import { myBus } from '../infrastructure/myBus'

let template = `
  <style>
    div { 
      margin-right: 10px; 
      display: inline;
    }
    button {
      background-color: springgreen;
      font-size: 12pt;
      border: none;
      padding: 5px;
    }
  </style>

  <div class="container">
    <button id='save'>Guardar</button>
  </div>
`

export default class SaveButton extends Component {
  constructor() {
    super(template, myBus)
    
    let button_element = this.shadowroot.querySelector('#save')

    button_element.addEventListener('click', () => {
      this.bus.publish('save.button.clicked')
    })

  }

}