export const service = (bus) => {
  let data = [
    { id: 1, value: 'aceite esencial' },
    { id: 2, value: 'alimento' },
    { id: 3, value: 'infusión' }
  ]
  
  return { start : () => bus.publish('types.loaded', data) }
}